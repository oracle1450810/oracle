# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414416

姓名：唐佳丽

班级：20软工4搬

## 实验内容：

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

1. 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
2. 设计权限及用户分配方案。至少两个用户。
3. 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
4. 设计一套数据库的备份方案。



## 实验步骤:

### 一、表及表空间设计方案：
为了提高性能和可靠性，我们可以将数据存储在多个表空间中，例如数据表空间、索引表空间和临时表空间。

#### 数据表空间：

用于存储数据表，包括商品、订单、顾客和销售记录等。

~~~sql
$sqlplus system/123@pdborcl

SQL>CREATE TABLESPACE data_tbs DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/data01.dbf'
  SIZE 500M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

--使用linux命令ls查看数据文件
SQL>!ls /home/oracle/app/oracle/oradata/orcl/pdborcl/data01.dbf -lh

~~~

![pict1](./pict1.png)

#### 索引表空间：

用于存储索引

~~~sql
SQL>CREATE TABLESPACE index_tbs DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborclindex01.dbf'
  SIZE 400M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
~~~

![pict2](./pict2.png)



#### 临时表空间：

用于存储临时数据，例如排序和聚合操作时使用的临时表。

~~~sql
SQL>CREATE TABLESPACE temporary_tbs DATAFILE
'/home/oracle/app/oracle/oradata/orcl/pdborcl/temporary01.dbf'
  SIZE 300M AUTOEXTEND OFF
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
~~~

![pict3](./pict3.png)



#### 查询表空间：

~~~sql
$ sqlplus system/123@pdborcl
SQL>
SELECT TABLESPACE_NAME,STATUS,CONTENTS,LOGGING FROM dba_tablespaces;

SELECT a.tablespace_name "表空间名",Total "大小",
 free "剩余",( total - free )"使用",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;

~~~

![pict4](./pict4.png)

![pict5](./pict5.png)
### 二、创建表：

在hr数据库下创建表。

~~~sql
$sqlplus hr/123@pdborcl
~~~

#### 商品表（products）：

包含商品的信息，商品ID、名称、描述、价格、库存。

~~~sql
CREATE TABLE products (
product_id NUMBER(10) PRIMARY KEY,
name VARCHAR2(100),
description VARCHAR2(4000),
price NUMBER(10,2),
stock NUMBER(10)
)
TABLESPACE data_tbs;
~~~

![pict6](./pict6.png)

插入4万条数据：

~~~sql
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 40000 LOOP
    INSERT INTO products (product_id, name, description, price, stock)
    VALUES (i, 'Product ' || i, 'Description ' || i, (i - 20000) * 0.99, (i - 20000) * 10);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
~~~

![pict7](./pict7.png)
#### 顾客表（customers）：

包含顾客的信息，顾客ID、姓名、地址、联系方式。

~~~sql
CREATE TABLE customers (
customer_id NUMBER(10) PRIMARY KEY,
name VARCHAR2(100),
address VARCHAR2(200),
phone_number VARCHAR2(20),
points NUMBER(10)
)
TABLESPACE data_tbs;
~~~

![pict8](./pict8.png)

插入2万条数据：

~~~sql
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 20000 LOOP
    INSERT INTO customers (customer_id, name, address, phone_number, points)
    VALUES (i, 'Customer ' || i, 'Address ' || i, '1234567890', i * 100);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
~~~

![pict9](./pict9.png)



#### 订单表（porders）：

包含订单的信息，订单ID、顾客ID、下单时间、总价。

~~~sql
CREATE TABLE porders (
order_id NUMBER(10) PRIMARY KEY,
customer_id NUMBER(10),
order_date TIMESTAMP,
total_price NUMBER(10,2),
CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers (customer_id)
)
TABLESPACE data_tbs;
~~~
![pict10](./pict10.png)

插入2万条数据：

~~~sql
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 20000 LOOP
    INSERT INTO porders (order_id, customer_id, order_date, total_price)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 20001)), SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)), i * 10.99);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
~~~

![pict11](./pict11.png)



#### 销售记录表（sales）：

记录每次销售的信息，包括订单ID、商品ID、销售数量、销售价格。

~~~sql
CREATE TABLE sales (
sale_id NUMBER(10) PRIMARY KEY,
order_id NUMBER(10),
product_id NUMBER(10),
sale_quantity NUMBER(10),
sale_price NUMBER(10,2),
CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES porders (order_id),
CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products (product_id)
)
TABLESPACE data_tbs;
~~~

![pict12](./pict12.png)

插入2万条数据：

~~~sql
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 20000 LOOP
    INSERT INTO sales (sale_id, order_id, product_id, sale_quantity, sale_price)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 20001)), TRUNC(DBMS_RANDOM.VALUE(1, 20001)), i * 2, i * 5.99);
    i := i + 1;
  END LOOP;
  COMMIT;
END;
~~~

![pict13](./pict13.png)

### 三、权限及用户分配方案：
为了保证数据库安全性，我们创建两个用户：一个用于管理数据库，另一个用于应用程序访问。管理员用户可以拥有完全的权限，应用程序用户只能访问必需的表和存储过程。

#### 创建管理员用户

~~~sql
$ sqlplus system/123@pdborcl

-- 创建管理员用户
CREATE USER admin_user IDENTIFIED BY 123
DEFAULT TABLESPACE data_tbs;

-- 赋予管理员权限
GRANT CREATE SESSION, CREATE TABLE, CREATE PROCEDURE, CREATE SEQUENCE, CREATE TRIGGER, CREATE VIEW TO admin_user;

~~~

![pict14](./pict14.png)

#### 创建应用程序用户

~~~sql

-- 创建应用程序用户
CREATE USER app_user IDENTIFIED BY 123
DEFAULT TABLESPACE data_tbs;

-- 赋予应用程序用户权限
GRANT CREATE SESSION TO app_user;

GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON porders TO app_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales TO app_user;

CREATE ROLE order_management;

GRANT EXECUTE ON create_order TO order_management;
GRANT EXECUTE ON cancel_order TO order_management;

GRANT order_management TO app_user;
~~~

![pict15](./pict15.png)



### 四、程序包设计：

创建一个名为sales_pkg的程序包，其中包含以下存储过程和函数：

1. create_order函数：用于创建新订单，接收一个customer_id和一个product_ids数组作为参数。该函数会计算订单的总价，并将订单和订单详情插入到porders和sales表中。
2. cancel_order存储过程：用于取消订单，接收一个order_id作为参数。该存储过程会删除订单详情和订单记录。
3. get_top_products函数：用于获取销售量前十的产品，不接收任何参数。该函数会返回一个游标，包含了销售量前十的产品信息。

~~~sql
CREATE OR REPLACE PACKAGE sales_pkg AS
  FUNCTION create_order(customer_id IN NUMBER, product_ids IN NUMBER_ARRAY) RETURN NUMBER;
  PROCEDURE cancel_order(order_id IN NUMBER);
  FUNCTION get_customer_sales(customer_id IN NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION get_product_sales(product_id IN NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION get_top_products RETURN SYS_REFCURSOR;
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_pkg AS

  FUNCTION create_order(customer_id IN NUMBER, product_ids IN NUMBER_ARRAY) RETURN NUMBER AS
    order_id NUMBER;
    total_price NUMBER;
  BEGIN
    -- 计算订单总价
    SELECT SUM(price) INTO total_price FROM products WHERE product_id IN (SELECT * FROM TABLE(product_ids));
    -- 插入新订单
    INSERT INTO porders (order_id, customer_id, order_date, total_price) VALUES (porders_seq.NEXTVAL, customer_id, SYSDATE, total_price);
    SELECT porders_seq.CURRVAL INTO order_id FROM DUAL;
    -- 插入订单详情
    FOR i IN 1..product_ids.COUNT LOOP
      INSERT INTO sales (sale_id, order_id, product_id, sale_quantity, sale_price) VALUES (sales_seq.NEXTVAL, order_id, product_ids(i), 1, (SELECTprice FROM products WHERE product_id = product_ids(i)));
    END LOOP;
    COMMIT;
    RETURN order_id;
  END create_order;

  PROCEDURE cancel_order(order_id IN NUMBER) AS
  BEGIN
    -- 删除订单详情
    DELETE FROM sales WHERE order_id = order_id;
    -- 删除订单
    DELETE FROM porders WHERE order_id = order_id;
    COMMIT;
  END cancel_order;

  FUNCTION get_top_products RETURN SYS_REFCURSOR AS
    top_products_cur SYS_REFCURSOR;
  BEGIN
    OPEN top_products_cur FOR
      SELECT p.product_id, p.product_name, SUM(s.sale_quantity) AS total_quantity
      FROM products p
      JOIN sales s ON p.product_id = s.product_id
      GROUP BY p.product_id, p.product_name
      ORDER BY total_quantity DESC
      FETCH FIRST 10 ROWS ONLY;
    RETURN top_products_cur;
  END get_top_products;

END sales_pkg;
/
~~~

![pict16](./pict16.png)

#### 调用create_order函数，创建订单：

~~~sql
DECLARE
  customer_id NUMBER := 1;
  product_ids sales_pkg.number_array := sales_pkg.number_array(1, 2, 3);
  order_id NUMBER;
BEGIN
  order_id := sales_pkg.create_order(customer_id, product_ids);
  DBMS_OUTPUT.PUT_LINE('New order created with ID ' || order_id);
END;
~~~

![pict17](./pict17.png)

#### 调用cancel_order存储过程，取消订单：

~~~sql
DECLARE
  order_id NUMBER := 1;
BEGIN
  sales_pkg.cancel_order(order_id);
  DBMS_OUTPUT.PUT_LINE('Order ' || order_id || ' cancelled');
END;/
~~~

![pict18](./pict18.png)



### 五、数据库备份方案：
完整备份和完全恢复

#### 数据库切换到归档日志模式

~~~sql
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;

~~~

![pict19](./pict19.png)

![pict20](./pict20.png)

![pict21](./pict21.png)


#### 删除数据文件

~~~sql
RMAN> host "rm /home/oracle/app/oracle/oradata/orcl/temp01.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/temp01.dbf";
~~~

![pict22](./pict22.png)

![pict23](./pict23.png)



![pict24](./pict24.png)

![pict25](./pict25.png)


#### 恢复删除的文件

~~~sql
RMAN> SHUTDOWN IMMEDIATE;
RMAN> SHUTDOWN ABORT;
RMAN> STARTUP MOUNT;
RMAN> RESTORE DATABASE;
RMAN> recover database;
RMAN> ALTER DATABASE OPEN;
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/temp01.dbf";
~~~

![pict26](./pict26.png)
![pict27](./pict27.png)
![pict28](./pict28.png)
![pict29](./pict29.png)

## 实验总结

在本次实验中，我学习了如何设计一个基于Oracle数据库的商品销售系统的数据库设计方案，并实现了一些复杂的业务逻辑和自动备份数据库的方案。这次实验让我受益匪浅，我总结了以下几点收获与体会：

1. 学习了数据库设计的基本知识：在本次实验中，我学习了如何设计一个基于Oracle数据库的商品销售系统的数据库设计方案，包括表及表空间设计方案、权限及用户分配方案、存储过程和函数的设计等。这些知识对我今后从事数据库开发和管理工作都非常有用。

2. 掌握了PL/SQL编程的基本技能：在实现存储过程和函数的过程中，我学习了PL/SQL编程语言的基本语法和使用方法。这让我对数据库编程有了更深入的了解，同时也提高了我的编程能力。

3. 学会了如何管理用户和权限：在本次实验中，我学习了如何创建不同的用户，并为不同用户分配不同的权限。这是一个非常重要的数据库管理技能，可以提高系统的安全性和稳定性。

4. 掌握了数据库备份和恢复的基本方法：在本次实验中，我学习了如何使用Oracle提供的备份工具来备份数据库，并了解了如何定期运行备份脚本并恢复数据库。这是一个非常重要的数据库管理技能，可以保护数据免受意外损坏和数据丢失的风险。

总之，通过本次实验，我不仅学习了数据库设计、PL/SQL编程、用户和权限管理、备份和恢复等方面的知识和技能，还深刻体会到了数据库管理的重要性和挑战性。这对我今后从事数据库开发和管理工作都非常有帮助。







