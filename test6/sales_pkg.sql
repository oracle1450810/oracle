--------------------------------------------------------
--  文件已创建 - 星期一-五月-22-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SALES_PKG
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "HR"."SALES_PKG" AS
  TYPE number_array IS TABLE OF NUMBER;
  FUNCTION create_order(customer_id IN NUMBER, product_ids IN number_array) RETURN NUMBER;
  PROCEDURE cancel_order(order_id IN NUMBER);
  FUNCTION get_top_products RETURN SYS_REFCURSOR;
END sales_pkg;

/
