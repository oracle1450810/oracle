# 学号：202010414416             姓名：唐佳丽

## 实验1：SQL语句的执行计划分析与优化指导

### 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

### 实验数据库和用户

数据库：pdborcl

用户：sys和hr

### 实验内容

1. 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
2. 查询IT部门工资高于2000的员工的全部信息。
3. 分析SQL查询语句，判断最优语句。
4. 通过sqldeveloper的优化指导工具对sql语句进行优化，并查看是否有优化建议。

### 实验过程

#### 1、向hr用户授予视图权限

~~~sql
[oracle@oracle1 ~]$ sqlplus sys/123@localhost/pdborcl as sysdba
SQL*Plus: Release 12.2.0.1.0 Production on 星期一 3月 20 16:53:05 2023
Copyright (c) 1982, 2016, Oracle.  All rights reserved.
连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production
SQL> @$ORACLE_HOME/sqlplus/admin/plustrce.sql
SQL> 
SQL> drop role plustrace;
角色已删除。
SQL> create role plustrace;
角色已创建。
SQL> 
SQL> grant select on v_$sesstat to plustrace;
授权成功。
SQL> grant select on v_$statname to plustrace;
授权成功。
SQL> grant select on v_$mystat to plustrace;
授权成功。
SQL> grant plustrace to dba with admin option;
授权成功。
SQL> 
SQL> set echo off
SQL> GRANT plustrace TO hr
  2  
SQL> GRANT plustrace TO hr;
授权成功。
SQL> GRANT SELECT ON v_$sql TO hr;
授权成功。
SQL> GRANT SELECT ON v_$sql_plan TO hr;
授权成功。
SQL> GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
授权成功。
SQL> GRANT SELECT ON v_$session TO hr;
授权成功。
SQL> GRANT SELECT ON v_$parameter TO hr;
授权成功。
SQL> 

~~~

#### 2、查询IT部门和Sales部门工资高于2000的员工的全部信息

~~~sql
SQL> exit
从 Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production 断开
[oracle@oracle1 ~]$ sqlplus hr/123@localhost/pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期一 3月 20 20:35:51 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期一 3月  20 2023 16:06:46 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> set autotrace on
SQL> SELECT * FROM employees,departments WHERE departments.department_id=employees.department_id  and employees.salary>=12000 and departments.department_name in ('IT','Sales');

EMPLOYEE_ID FIRST_NAME		 LAST_NAME
----------- -------------------- -------------------------
EMAIL			  PHONE_NUMBER	       HIRE_DATE	   JOB_ID
------------------------- -------------------- ------------------- ----------
    SALARY COMMISSION_PCT MANAGER_ID DEPARTMENT_ID DEPARTMENT_ID
---------- -------------- ---------- ------------- -------------
DEPARTMENT_NAME 	       MANAGER_ID LOCATION_ID
------------------------------ ---------- -----------
	145 John		 Russell
JRUSSEL 		  011.44.1344.429268   2004-10-01 00:00:00 SA_MAN
     14000	       .4	 100		80	      80
Sales				      145	 2500

EMPLOYEE_ID FIRST_NAME		 LAST_NAME
----------- -------------------- -------------------------
EMAIL			  PHONE_NUMBER	       HIRE_DATE	   JOB_ID
------------------------- -------------------- ------------------- ----------
    SALARY COMMISSION_PCT MANAGER_ID DEPARTMENT_ID DEPARTMENT_ID
---------- -------------- ---------- ------------- -------------
DEPARTMENT_NAME 	       MANAGER_ID LOCATION_ID
------------------------------ ---------- -----------
	146 Karen		 Partners
KPARTNER		  011.44.1344.467268   2005-01-05 00:00:00 SA_MAN
     13500	       .3	 100		80	      80
Sales				      145	 2500


EMPLOYEE_ID FIRST_NAME		 LAST_NAME
----------- -------------------- -------------------------
EMAIL			  PHONE_NUMBER	       HIRE_DATE	   JOB_ID
------------------------- -------------------- ------------------- ----------
    SALARY COMMISSION_PCT MANAGER_ID DEPARTMENT_ID DEPARTMENT_ID
---------- -------------- ---------- ------------- -------------
DEPARTMENT_NAME 	       MANAGER_ID LOCATION_ID
------------------------------ ---------- -----------
	147 Alberto		 Errazuriz
AERRAZUR		  011.44.1344.429278   2005-03-10 00:00:00 SA_MAN
     12000	       .3	 100		80	      80
Sales				      145	 2500



执行计划
----------------------------------------------------------
Plan hash value: 1021246405

--------------------------------------------------------------------------------
------------------

| Id  | Operation		     | Name		 | Rows  | Bytes | Cost
(%CPU)| Time	 |

--------------------------------------------------------------------------------
------------------

|   0 | SELECT STATEMENT	     |			 |    11 |   990 |     4
   (0)| 00:00:01 |

|   1 |  NESTED LOOPS		     |			 |    11 |   990 |     4
   (0)| 00:00:01 |

|   2 |   NESTED LOOPS		     |			 |    20 |   990 |     4
   (0)| 00:00:01 |

|*  3 |    TABLE ACCESS FULL	     | DEPARTMENTS	 |     2 |    42 |     3
   (0)| 00:00:01 |

|*  4 |    INDEX RANGE SCAN	     | EMP_DEPARTMENT_IX |    10 |	 |     0
   (0)| 00:00:01 |

|*  5 |   TABLE ACCESS BY INDEX ROWID| EMPLOYEES	 |     5 |   345 |     1
   (0)| 00:00:01 |

--------------------------------------------------------------------------------
------------------
Predicate Information (identified by operation id):
---------------------------------------------------

   3 - filter("DEPARTMENTS"."DEPARTMENT_NAME"='IT' OR
	      "DEPARTMENTS"."DEPARTMENT_NAME"='Sales')
   4 - access("DEPARTMENTS"."DEPARTMENT_ID"="EMPLOYEES"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY">=12000)

Note
-----
   - this is an adaptive plan
统计信息
----------------------------------------------------------
	 18  recursive calls
	  0  db block gets
	 44  consistent gets
	  0  physical reads
	  0  redo size
   1887  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  3  rows processed

SQL> 
~~~

![pict1](./pict1.png)

#### 3、优化

~~~sql
SELECT * FROM employees,departments 
WHERE departments.department_id=employees.department_id  
and employees.salary>=12000 
and departments.department_name ='IT' 
or departments.department_name='Sales';
~~~

![pict2](./pict2.png)



### 实验结果及分析

在进行优化时，通过终端的代码演示，会出现相应优化提示，在数据库查询语句中对比运行时间，发现时间更短。该查询语句在语句一的基础上进行了优化，将查询的条件部门名’IT’和’Sales’换成对应部门的id进行查询，使用部门名查询对应的部门id作为子查询而得到的结果作为外层查询条件，这样来查询结果的准确性更高，查询效率也更高。















