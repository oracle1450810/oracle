# 实验4：PL/SQL语言打印杨辉三角

学号：202010414416

姓名：唐佳丽

班级：20软工4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 实验步骤

### 1、hr用户下创建存储过程YHTriange，存储过程接受行数N为参数

~~~sql
CREATE OR REPLACE PROCEDURE YHTriangle(N IN NUMBER) IS
    TYPE t_number IS VARRAY(100) OF NUMBER NOT NULL;
    i INTEGER;
    j INTEGER;
    spaces VARCHAR2(30) :=' '; --三个空格，用于打印时分隔数字
    rowArray t_number := t_number();
BEGIN
    DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行
    DBMS_OUTPUT.PUT(RPAD(1,9,' ')); --先打印第2行
    DBMS_OUTPUT.PUT(RPAD(1,9,' ')); --打印第一个1
    DBMS_OUTPUT.PUT_LINE(''); --打印换行
    
    --初始化数组数据
    FOR i IN 1 .. N LOOP
        rowArray.extend;
    END LOOP;
    
    rowArray(1) := 1;
    rowArray(2) := 1;
    
    FOR i IN 3 .. N --打印每行，从第3行起
    LOOP
        rowArray(i) := 1;
        j := i - 1;
        
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        WHILE j > 1 LOOP
            rowArray(j) := rowArray(j) + rowArray(j-1);
            j := j - 1;
        END LOOP;
        
        --打印第i行
        FOR j IN 1 .. i LOOP
            DBMS_OUTPUT.PUT(RPAD(rowArray(j),9,' ')); --打印第一个1
        END LOOP;
        
        DBMS_OUTPUT.PUT_LINE(''); --打印换行
    END LOOP;
END;
~~~

![pict1](./pict1.png)



![pict2](./pict2.png)

## 2、在工作表中运行存储过程，打印杨辉三角

~~~sql
SET SERVEROUTPUT ON;
EXEC YHTriangle(10);
~~~

![pict3](./pict3.png)

## 实验总结

在实验过程中，我们首先认真阅读和理解了给定的源代码，并根据要求将其转换为存储过程。在编写存储过程的过程中，我们使用了Oracle的PL/SQL语言，并使用了临时表、循环、条件判断等PL/SQL的基本语法和结构。最终，我们成功地将源代码转换为存储过程，并将其存储在数据库中以便后续使用。

通过本次实验，我们掌握了Oracle PL/SQL语言和存储过程的基本语法和结构，并学会了如何将源代码转换为存储过程并将其存储在数据库中。这对于我们日后在数据库管理和开发中的实际工作中将会非常有用。
